# Canter Full-stack Recruitment Task

  This is a backend-centric task for a full-stack position. This repository
  contains some code to get you started.

  The `backend` folder contains a Scala+Play project skeleton. The code can
  be ran using the `sbt run` command.

  The `frontend` folder contains an Angular project that contains a functional
  frontend for the task. The code can be ran using the `npm install; npm start`
  commands.

## THE TASK

  The task is to develop a Single Page (SPA) application for listing and manipulating
  products, including all CRUD functions (create, read, update and delete).
  See images for reference.
  
  Views:
  Product list view contains edit and delete buttons and a link to create a new product.
  Edit form for a single product.
  
  ![Form](./images/form.png)
  
  
  ![List](./images/list.png)

  A working frontend implementation is provided. Your task is to implement a
  complementing backend.

##  BACKEND

  Implement a REST JSON API with data persisted to a database. 

  Example JSON representing a single product:
  
    {
      "id": 32123,
      "name": "CanterBook ÜberPro",
      "category": "laptops",
      "code": "123",
      "price": 12.13,
      "details": [
        {
          "key": "Cpu",
          "value": "16 core, Adeona processor"
        },
        {
          "key": "Display",
          "value": "Yes"
        }
      ]
    }


  REST API specification:

    POST    /products                   // Create new product
    GET     /products                   // List all products
    GET     /products/<id>              // Get one product
    PUT     /products/<id>              // Update product
    DELETE  /products/<id>              // Remove product

##  FRONTEND

  The frontend code works, but is not very clean and easy to read. It doesn't
  match the provided design and may contain bugs too. For bonus points please
  take a look at the code and refactor it to be cleaner and more maintainable. 
  Fixing the layout and styling would be nice too.
    
