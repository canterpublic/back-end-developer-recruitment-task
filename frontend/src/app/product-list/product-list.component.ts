import { Component } from '@angular/core';
import { PRODUCTS, IProductType } from 'src/Mock';
import { Observable, of } from 'rxjs';
import { BASE_URL } from '../APIService';
import { catchError, map, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';


@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})

export class ProductListComponent {
  dataSource: IProductType[]

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
    return this.getData().subscribe(res => { this.dataSource = res });
  }


  getData(): Observable<IProductType[]> {
    console.log('fetching')
    const headers = new HttpHeaders({ 'Access-Control-Allow-Origin': '*' });
    const response = this.http.get<IProductType[]>(`${BASE_URL}`, {
      headers: headers
    })
      .pipe(
        catchError
          (this.handleError<[]>('getData', []))
      );

    console.log('response= ', response);
    return response;
  }
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error('Error is=', error); // log to console instead
      return of(result as T);
    };
  }

  onEdit(e) {
    // console.log('Editing', e);
    // console.log(JSON.stringify(e));
    this.router.navigate(['add', { data: JSON.stringify(e) }])
    // document.getElementById('').values()
    // console.log(document.getElementById('code'))
  }

  onDelete(e) {
    console.log('deleting', e)
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      "Access-Control-Allow-Methods": "DELETE, POST, GET, OPTIONS"
    });

    this.http.delete(`${BASE_URL + e}`, {
      headers: headers
    }).subscribe(
      () => {this.getData().subscribe(res => { this.dataSource = res })}, 
    )
  }
  
}

export interface PeriodicElement {
  category: string;
  code: number;
  name: number;
  price: string;
}
